import time

import pandas as pd
import math
import ast
import sys
import re

# import time

mg_column_name = 'mg'  # stands for G in rules file, value of G present in mg column in data file
gram_column_name = 'gram'
ml_column_name = 'ml'
units_column_name = 'units'

rules_file_loc = r'data/reference_files/su_calculation_rule.xlsx'
output_file_loc = r'data/output/process_su_output_test_all_1.csv'


def preprocess_raw_data(raw_data_row):
    raw_data_row['error'] = ''

    # MG
    mg_v = raw_data_row['mg']
    if ',' in str(mg_v) or ';' in str(mg_v):
        mg_v = re.split('[,;]', mg_v)
        temp_mg_v = set(mg_v)
        if len(temp_mg_v) > 1:
            mg_v = ''
        else:
            mg_v = float(mg_v[0])
        raw_data_row['mg'] = mg_v
    elif mg_v != '':
        raw_data_row['mg'] = float(mg_v)

    # Gram
    gram_v = raw_data_row['gram']
    if ',' in str(gram_v) or ';' in str(gram_v):
        gram_v = re.split('[,;]', gram_v)
        temp_gram_v = set(gram_v)
        if len(temp_gram_v) > 1:
            gram_v = ''
        else:
            gram_v = float(gram_v[0])
        raw_data_row['gram'] = gram_v
    elif mg_v != '':
        raw_data_row['gram'] = float(gram_v)

    # ML
    ml_v = raw_data_row['ml']
    if ',' in str(ml_v) or ';' in str(ml_v):
        ml_v = re.split('[,;]', ml_v)
        temp_ml_v = set(ml_v)
        if len(temp_ml_v) > 1:
            ml_v = ''
        else:
            ml_v = float(ml_v[0])
        raw_data_row['ml'] = ml_v
    elif ml_v != '':
        raw_data_row['ml'] = float(ml_v)

    # Units
    units_v = raw_data_row['units']
    if pd.isna(units_v):
        units_v = 1
    elif int(units_v) == -1:
        units_v = 1
    elif ',' in str(units_v) or ';' in str(units_v):
        units_v = re.split('[,;]', units_v)
        units_v = ' * '.join(units_v)
        units_v = eval(units_v)
    raw_data_row['units'] = float(units_v)

    return raw_data_row


def calc_su(raw_data: pd.DataFrame, rules_file: pd.DataFrame) -> pd.DataFrame:
    for df_model_row_index, data_row in raw_data.iterrows():  # iterate through each row of data
        ################
        # # SU logic # #
        ################
        if data_row['transcoding'] != 'True':   # Exception, False
            continue

        # mg
        # mg_v = data_row['mg']
        # if pd.isna(mg_v):
        #     mg_v = 0
        # elif ',' in str(mg_v) or ';' in str(mg_v):
        #     mg_v = re.split('[,;]', mg_v)
        #     mg_v = mg_v[0]

        # gram
        gram_v = data_row['gram']
        if pd.isna(gram_v):
            gram_v = 0
        elif ',' in str(gram_v) or ';' in str(gram_v):
            gram_v = re.split('[,;]', gram_v)
            gram_v = gram_v[0]

        # mg_v = float(mg_v) / 1000
        # g_v = mg_v

        ml_v = data_row['ml']
        units_v = data_row['units']
        sub_error_message = None
        # g_v = 1
        # if data_row['mg'] != '':
        #     try:
        #         g_v = float(data_row['mg']) / 1000
        #     except Exception as e:
        #         print(e)
        #         sub_error_message = e
        # else:
        #     g_v = '' # data_row['mg']

        try:
            # #  extract from column SU, G and ML, default 1
            # # SU_model
            for _, rules_row in rules_file.iterrows():  # iterate through each row of rule
                rule_found = True
                for row_index in range(9):  # iterate through each cell of rules row
                    column_name = rules_row.index[row_index]
                    column_value = rules_row[row_index]
                    if pd.isna(column_value):
                        continue

                    data_val = data_row[column_name]
                    data_val = str(data_val).lower()

                    if '[' in str(column_value):  # is a list-> other operations
                        column_value = ast.literal_eval(column_value)
                        operation = column_value[0]
                        value = column_value[1]
                        value = [str(v).lower().strip() for v in value]

                        if '!' in operation:
                            # if operation == '!=': #  '!' in
                            if data_val in value:
                                rule_found = False
                                break
                        elif '=' in operation:
                            if column_name == ml_column_name:
                                if ml_v != float(value[0]):
                                    rule_found = False
                                    break
                            elif column_name == mg_column_name:
                                if gram_v != float(value[0]):
                                    rule_found = False
                                    break
                            elif data_val not in value:
                                rule_found = False
                                break
                        elif operation == 'contains':
                            if not any(string in data_val for string in value):
                                rule_found = False
                                break
                        elif operation == '<':
                            if column_name == ml_column_name:
                                if ml_v >= float(value[0]):
                                    rule_found = False
                                    break
                            elif column_name == mg_column_name:
                                if gram_v >= float(value[0]):
                                    rule_found = False
                                    break
                            elif float(data_val) >= float(value[0]):
                                rule_found = False
                                break
                        elif operation == '>':
                            if column_name == ml_column_name:
                                if ml_v <= float(value[0]):
                                    rule_found = False
                                    break
                            elif column_name == mg_column_name:
                                if gram_v <= float(value[0]):
                                    rule_found = False
                                    break
                            elif float(data_val) <= float(value[0]):
                                rule_found = False
                                break
                        elif operation == 'multiple =':
                            if data_val not in value:
                                rule_found = False
                                break
                    elif data_val != column_value.lower().strip():
                        rule_found = False
                        break

                if rule_found:
                    raw_data.loc[df_model_row_index, 'rule_number'] = rules_row['S.No.']

                    dividend = str(rules_row['Dividend'])
                    divisor = str(rules_row['Divisor'])

                    # check if there are multiple rules for same nfc, if this is the first one and has blank then skip
                    if pd.notna(rules_row['Multiple rules']) and str(
                            rules_row['Multiple rules']).lower().strip() == 'yes':
                        # check if key column is null
                        if dividend == 'SU' or dividend == 'SU[0]':
                            if pd.isna(data_row[units_column_name]) or data_row[units_column_name] == '':
                                continue  # skip this rule and wait for the next one
                        elif dividend == 'G':
                            if pd.isna(data_row[gram_column_name]) or data_row[gram_column_name] == '':
                                continue  # skip this rule and wait for the next one
                        elif dividend == 'ML':
                            if pd.isna(data_row[ml_column_name]) or data_row[ml_column_name] == '':
                                continue  # skip this rule and wait for the next one
                    if dividend == 'SU':
                        dividend = units_v
                    elif dividend == 'SU[0]':
                        dividend = \
                            ('1' if pd.isna(data_row[units_column_name]) else str(
                                data_row[units_column_name])).lower().strip().replace(
                                ' ', '').split('x')[0]
                        dividend = float(''.join(char for char in dividend if char.isdigit() or char in '.'))
                    elif dividend == 'G':
                        dividend = gram_v
                    elif dividend == 'ML':
                        dividend = ml_v
                    else:
                        dividend = float(dividend)

                    if divisor == 'SU':
                        divisor = units_v
                    elif divisor == 'G':
                        divisor = gram_v
                    elif divisor == 'ML':
                        divisor = ml_v
                    else:
                        divisor = float(divisor)

                    # error management
                    if math.isnan(divisor):
                        raw_data.loc[df_model_row_index, 'error'] \
                            = f"Divisor is null. Rule- {rules_row['Dividend']}/{rules_row['Divisor']} "
                        break
                    elif math.isnan(dividend):
                        raw_data.loc[df_model_row_index, 'error'] \
                            = f"dividend is null. Rule- {rules_row['Dividend']}/{rules_row['Divisor']} "
                        break
                    elif dividend == 0:
                        raw_data.loc[df_model_row_index, 'error'] \
                            = f"dividend is 0. Rule- {rules_row['Dividend']}/{rules_row['Divisor']} "
                        break
                    elif dividend == '' or divisor == '':
                        raw_data.loc[df_model_row_index, 'error'] \
                            = f"Insufficient data found. Rule- {rules_row['Dividend']}/{rules_row['Divisor']} "
                        break
                    calc_su_value = dividend / divisor
                    if not ((str(rules_row['Dividend']) == 'SU') or (str(rules_row['Dividend']) == 'SU[0]') or (
                            str(rules_row['Divisor']) == 'SU') or str(data_row['Unit']).lower() == 'su' or (
                                    (str(rules_row['Dividend']) == '1') and (str(rules_row['Divisor']) == '1'))):
                        calc_su_value = calc_su_value * units_v

                    raw_data.loc[df_model_row_index, 'SU_C'] = calc_su_value  # dividend / divisor
                    break
        except Exception as e:
            print(
                "******************************* Error Occurred @ process_su.py !! ********************************")
            if pd.notna(sub_error_message):
                e = sub_error_message
            print(e)
            # logger_code.error(traceback.print_exc(file=sys.stdout))
            raw_data.loc[df_model_row_index, 'error'] = 'Error' + str(sys.exc_info()[2].tb_lineno) + " : " + str(e)

    return raw_data.copy()


def main(raw_file_path='Coded.csv' or str) -> None:
    """
        reads data and rules file and calls calc_su function which
        calculates SU based on the rules and saves the fle at the given location.

        Parameters:
        arg1 (str): data file name

        Returns:
            None
    """
    start_time = time.time()
    df_data = pd.read_csv(raw_file_path)
    df_data['transcoding'] = df_data['transcoding'].astype(str)
    rules_file = pd.read_excel(rules_file_loc, sheet_name='SU', engine='openpyxl')
    df_data = df_data.apply(preprocess_raw_data, axis=1)
    df_final = calc_su(raw_data=df_data, rules_file=rules_file)
    df_final.to_csv(output_file_loc, index=False)
    end_time = time.time()
    print(end_time - start_time)
    return None


# apply
def fill_su_for_unit_packs(row):
    # numerator = None
    # denominator = None
    ret_val = -1
    dividend = re.split('[,;]', row['Dividend'])
    divisor = re.split('[,;]', row['Divisor'])
    count = len(divisor)
    for element_index in range(count):
        if dividend[element_index] == 'SU':
            numerator = row['units']
        elif dividend[element_index] == 'G':
            if row['mg'] != '':
                numerator = float(row['mg']) / 1000
            else:
                numerator = row['mg']
        elif dividend[element_index] == 'ML':
            numerator = row['ml']
        elif dividend[element_index] == 'mg':
            numerator = row['mg']
        else:
            numerator = float(dividend[element_index])

        # denominator
        if divisor[element_index] == 'SU':
            denominator = row['units']
        elif divisor[element_index] == 'G':
            if row['mg'] != '':
                denominator = float(row['mg']) / 1000
            else:
                denominator = row['mg']
        elif divisor[element_index] == 'ML':
            denominator = row['ml']
        elif divisor[element_index] == 'mg':
            denominator = row['mg']
        else:
            denominator = float(divisor[element_index])

        if numerator and denominator:
            if numerator == '' or denominator == '':
                ret_val = f"Insufficient data found. Rule- {row['Dividend']}/{row['Divisor']}"
            else:
                ret_val = numerator / denominator
                return ret_val

    if count:
        ret_val = f"Insufficient data found. Rule- {row['Dividend']}/{row['Divisor']}"

    return ret_val


# def su_packs(raw_file_unit_pack, rules_file_unit_pack):
#     rules_file_unit_pack = rules_file_unit_pack.drop_duplicates()
#
#     rules_file_unit_pack['Dividend'] = rules_file_unit_pack['Dividend'].astype(str)
#     rules_file_unit_pack['Divisor'] = rules_file_unit_pack['Divisor'].astype(str)
#
#     rules_file_unit_pack = rules_file_unit_pack.groupby(['NFC123_Recode']).agg(
#         {'Dividend': ','.join, 'Divisor': ','.join}).reset_index(drop=False)
#
#     raw_file_unit_pack = pd.merge(raw_file_unit_pack, rules_file_unit_pack, how='left', left_on=['NFC123_Recode'],
#                                   right_on='NFC123_Recode')
#     raw_file_unit_pack['Dividend'] = raw_file_unit_pack['Dividend'].fillna('0')
#     raw_file_unit_pack['Divisor'] = raw_file_unit_pack['Divisor'].fillna('1')
#     raw_file_unit_pack['SU_C'] = raw_file_unit_pack.apply(fill_su_for_unit_packs, axis=1)
#
#     return raw_file_unit_pack

#
# def main(raw_file_path=r'.\data\output\Coded.csv'):
#     start_time = time.time()
#
#     df_data = pd.read_csv(raw_file_path)
#     rules_file = pd.read_excel(r".\data\test_run_rules.xlsx", sheet_name='SU', engine='openpyxl')
#
#     df_data['Unit'] = df_data['Unit'].apply(lambda x: x.strip())
#     df_data['Country'] = df_data['Country'].apply(lambda x: str(x).strip())
#     df_data['Country'] = df_data['Country'].apply(lambda x: str(x).strip())
#     df_data['Category'] = df_data['Category'].apply(lambda x: str(x).strip())
#     df_data['Pack_recode'] = df_data['Pack_recode'].apply(lambda x: str(x).strip())
#
#     df_data['transcoding'].fillna(False, inplace=True)
#     df_not_transcoding = df_data[~df_data['transcoding']]
#     df_data = df_data[df_data['transcoding']]
#
#     df_data = df_data.apply(preprocess_raw_data, axis=1)
#
#     raw_file_unit_pack = df_data[df_data['Unit'] == 'PACK'].copy()
#     raw_file_unit_pack = raw_file_unit_pack[~raw_file_unit_pack['NFC123_Recode'].isin(['DEK', 'DGJ'])]
#
#     rules_file_unit_pack = rules_file[rules_file['Unit'] == 'Pack']
#     rules_file_unit_pack = rules_file_unit_pack[~rules_file_unit_pack['NFC123_Recode'].isin(['DEK', 'DGJ'])]
#     rules_file_unit_pack = rules_file_unit_pack[['NFC123_Recode', 'Dividend', 'Divisor']]
#
#     df_packs = su_packs(raw_file_unit_pack, rules_file_unit_pack)
#
#     df_data = df_data[df_data['Unit'] != 'PACK']
#
#     df_final = calc_su(raw_data=df_data, rules_file=rules_file)
#
#     df_final = df_final.append(df_packs)
#     df_final = df_final.append(df_not_transcoding)
#     df_final.to_csv('data/output/process_su_output_4.csv', index=False)
#
#     end_time = time.time()
#     print(f"time taken for process_su: {end_time - start_time}")
# >>>>>>> 2692c1c67eb1aa54d844d7049a28f50c5e3b4972


if __name__ == '__main__':
    main()
