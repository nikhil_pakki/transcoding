import re

numeric_decimal_pattern = r'(?#matching numbers and decimals)(?:\d[\.\d]*|\d*\.\d+)'


re_flags = re.I | re.M

wt_values_pattern = re.compile(r'(?<=\b)(?# this is a word boundary)(?P<MG_value>(?# group name MG_value)(?<!X)(?:(?:\d[\.\d]*|\d*\.\d+)\s*[/+\-]\s*)*(?:\d[\.\d]*|\d*\.\d+))(?# 10/20 mg or 10+20 mg)\s*(?P<unit>MG|MCG|K|MU|Y|IU)(?=\W|$)(?# should end with MCG|K|MU|Y|IU|MG)|(?P<MG_Value>(?:(?:\d[\.\d]*|\d*\.\d+)\s*/\s*)+(?:\d[\.\d]*|\d*\.\d+)\s(?!ML|IU|LT))(?# matching 10/20/30 )')

# wt_values_pattern = re.compile(r'(?<=\b)(?# this is a word boundary)'
#                                r'(?P<MG_value>(?# group name MG_value)' +
#                                r'(?<!X)(?:{0}\s*[/+\-]\s*)*{0})(?# 10/20 mg or 10+20 mg)'.format(numeric_decimal_pattern) +
#                                r'\s*(?P<unit>M?G|MCG|K|MU|Y|IU)(?=\W|$)(?# should end with MCG|K|MU|Y|IU|MG)|'
#                                r'(?P<MG_Value>(?:{0}\s*/\s*)+{0}\s(?!ML|IU|LT))(?# matching 10/20/30 )'.format(numeric_decimal_pattern), re_flags)

gram_value_pattern = re.compile(r'(?<=\b)(?# this is a word boundary)(?P<G_value>(?# group name MG_value)(?<!X)(?:(?:\d[\.\d]*|\d*\.\d+)\s*[/+\-]\s*)*(?:\d[\.\d]*|\d*\.\d+))(?# 10/20 mg or 10+20 mg)\s*(?P<unit>G)(?=\W|$)(?# should end with MCG|K|MU|Y|IU|MG)')

vl_values_pattern = re.compile(r'(?<=[^\/\s])\s+(?P<ML_value>(?:\d|\.\d)[\-\d\.]*)\s*(?P<value_type>ML|LT|L)(?=\W|$)',
                               re_flags)

wt_p_vl_values_pattern = re.compile(r'\/\s*(?P<per_value>\d[\-\d\.]*|\d*)\s*(?P<value_type>ML)', re_flags)

# changed Units' regex for ''
units_values_pattern = re.compile(
    r"(\s\d+\s*\+\s*\d+\s+)(?!ML|MC?G|GM|G|K|MU|Y|IU|X|DOSE|D|DOS|\s)|(?<![/+.])[X\s]+((?:\d+\s*(?:(?:BL|ST|DO|UD)[\w\.]*)?\s*)?X\s*\d+\s*)|(?<![/+.])\s(\d[\d\s]*)$|\s+(\d+)\s*(DOSES?|DOS|D)(?=\W|$)|(?<!/)\s((?:\d+\s*X\s*|X\s*)?\d+)\s*(?:UD|DOS|ST)?\s*(?=\(|$)|(?<!/)\s+(\d+)\s+(?!ML|MC?G|GM|G|K|MU|Y|IU|X|DOSE|D|DOS|\+|\\|\/|\s)|(\d+\s*X\s*\d+)\s*UD|\sX\s(\d+)\s+(?!ML|MC?G|GM|G|K|MU|Y|IU|X|E|DOSE|D|DOS)")

# units_values_pattern = re.compile(r'(?<![/+.])[X\s]+((?:\d+\s*(?:(?:BL|ST|DO|UD)[\w\.]*)?\s*)?X\s*\d+\s*(?:(?:BL|ST|DO|UD)[\w\.]*)?)|'
#                                   r'(?<![/+.])\s(\d[\d\s]*)$|'
#                                   r'\s+(\d+)\s*(DOSES?|DOS|D)(?=\W|$)|'
#                                   r'(?<!/)\s((?:\d+\s*X\s*|X\s*)?\d+)\s*(?:UD|DOS|ST)?\s*(?=\(|$)|(?<!/)\s+(\d+)\s+('
#                                   r'?!ML|MC?G|GM|G|K|MU|Y|IU|X|DOSE|D|DOS)|(\d+\s*X\s*\d+)\s*UD|'
#                                   r'\sX\s(\d+)\s+(?!ML|MC?G|GM|G|K|MU|Y|IU|X|E|DOSE|D|DOS)|'
#                                   r'\s\d+\s*\+\s*\d+\s*(?=ST)',
#                                   re_flags)


multiplier_pattern = re.compile(r'(?<![/+.])\s\d+\s*(?:(?:BL|ST|DO|UD|D)[\w.]*)?\s*X\s*\d+\s*(?:(?:BL|ST|DO|UD|D)[\w.]*)?', re_flags)

"""
Dosage pattern is mainly to identify the below listed entries:
    FILM-COAT TB 100 MG 6 10 ['6', '10']
    TABLET 80 MG 4 7 ['4', '7']
    FCT 10/ 10 3 10 ['3', '10']
    TABLET EFF 12 ['12']
    (OR)
    SOLUT.NEBUL. 300 MG 56 5 DOS ['5']
"""
dosage_pattern = re.compile(r'(?<![/+.])\s\d[\d\s]*$|'
                            r'\s+(\d+)\s*(DOSES?|DOS|D)(?=\W|$)', re_flags)

number_pattern = re.compile(r'\d*\.\d+|\d+')
plus_pattern = re.compile(r'\+')
digit_pattern = re.compile(r'\d+')
