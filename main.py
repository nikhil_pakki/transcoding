import my_regex
import time
import warnings

import pandas as pd

warnings.filterwarnings('ignore')

DF = pd.DataFrame
to_numeric = pd.to_numeric


def mg_vals(row):
    found = my_regex.wt_values_pattern.search(row)
    mg_array = ''
    if found:
        # matches = my_regex.wt_values_pattern.findall(row)
        mg_array = []

        for match in my_regex.wt_values_pattern.finditer(row):
            value = match.group('MG_value')
            if not value:
                value = match.group('MG_Value')
            vals = my_regex.number_pattern.findall(value)
            unit = match.group('unit')
            if not unit:
                unit = 'MG'

            for val_index in range(len(vals)):
                if unit == 'G':
                    vals[val_index] = to_numeric(vals[val_index]) * 1000
                elif unit == 'MCG':
                    vals[val_index] = to_numeric(vals[val_index]) / 1000
                elif unit == 'Y':
                    vals[val_index] = to_numeric(vals[val_index]) / 1000

            vals = map(lambda x: str(x), vals)
            vals = ','.join(list(vals))
            mg_array.append(vals)
    return ';'.join(mg_array)


def gram_vals(row):
    found = my_regex.gram_value_pattern.search(row)
    gram_array = ''
    if found:
        # matches = my_regex.wt_values_pattern.findall(row)
        gram_array = []

        for match in my_regex.gram_value_pattern.finditer(row):
            value = match.group('G_value')
            if not value:
                value = match.group('G_Value')
            vals = my_regex.number_pattern.findall(value)
            # unit = match.group('unit')

            for val_index in range(len(vals)):
                vals[val_index] = to_numeric(vals[val_index])

            vals = map(lambda x: str(x), vals)
            vals = ','.join(list(vals))
            gram_array.append(vals)
    return ';'.join(gram_array)


def ml_vals(row):
    found = my_regex.vl_values_pattern.search(row)
    ml_array = ''
    # vals = ''
    if found:
        matches = my_regex.vl_values_pattern.findall(row)
        ml_array = []
        for match in matches:
            vals = my_regex.number_pattern.findall(match[0])
            unit = match[1]

            for val_index in range(len(vals)):
                if unit == 'L' or unit == 'LT':
                    vals[val_index] = str("{:.2f}".format(to_numeric(vals[val_index]) * 1000))
                # elif :
                #     vals[val_index] = str("{:.2f}".format(to_numeric(vals[val_index]) * 1000))
                else:
                    vals[val_index] = str("{:.2f}".format(to_numeric(vals[val_index])))

            # vals = map(lambda x: str(x), vals)
            vals = ','.join(list(vals))
            ml_array.append(vals)
    return ';'.join(ml_array)


def per_ml_vals(row):
    vals = my_regex.wt_p_vl_values_pattern.findall(row)
    per_ml_array = []
    for val in vals:
        if val[0] == '':
            val = '1'
        else:
            val = str(val[0]).strip()
        per_ml_array.append(val)
    per_ml_array = ','.join(per_ml_array)
    return per_ml_array


def unit_vals(row) -> int:
    found = my_regex.units_values_pattern.findall(row)
    # print(my_regex.units_values_pattern.findall(row))
    # finds = my_regex.units_values_pattern.findall(row)
    # finds = [y for x in finds for y in x if y.strip()]

    n_dos = -1
    if found:
        found = list(filter(''.__ne__, list(found[-1])))  # get the last match
    if found:
        match = found[0]
        vals = my_regex.number_pattern.findall(match)
        if len(vals) > 1 and '+' in match:
            n_dos = eval(match)
            return int(n_dos)

        vals = map(lambda x: str(x), vals)
        n_dos = -1
        vals = list(vals)
        if len(vals) > 0:
            n_dos = 1
            for val in vals:
                if float(val) != 0:
                    n_dos = n_dos * float(val)
    return int(n_dos)


def dosage_vals(row):  # pragma: no cover
    found = my_regex.dosage_pattern.search(row)
    finds = my_regex.dosage_pattern.findall(row)
    finds = [y for x in finds for y in x if y.strip()]

    if len(finds) > 1 and 'TAB' in row:
        print('--------------', row, finds)
    n_dos = -1
    # vals = ''
    if found:
        vals = my_regex.number_pattern.findall(found.group(0))
        if len(vals) > 1 and '+' in row:
            print(row, vals)
        elif len(vals) > 1:
            print('---', row, vals)
        vals = map(lambda x: str(x), vals)
        n_dos = -1
        vals = list(vals)
        if len(vals) > 0:
            n_dos = 1
            for val in vals:
                n_dos = n_dos * float(val)

    return n_dos


def multiplier_vals(row):
    found = my_regex.multiplier_pattern.search(row)
    ret_val = ''
    if found:
        match = found.group(0)
        ret_val = ','.join(sorted(my_regex.digit_pattern.findall(match)))
    return ret_val


def main(
        raw_file_path=r'D:\office_data\office_data\su_project\su_regex\data/cleaned_dmt_ref_transcode.csv'):
    df = pd.read_csv(raw_file_path)
    print(df.shape)
    df['raw_Pack_recode'] = df['Pack_recode']
    # df = df[1_000:10_000]
    # df = df.head(1_000)

    # checklist_df = pd.read_excel('20220421_Transcoding checklist.xlsx', sheet_name='Checklist metrics')
    # checklist_df.fillna('', inplace=True)
    # checklist_df = checklist_df.astype(str)
    # checklist_master_cat = checklist_df[checklist_df['MG'] == '']
    # no_mg_required_list = list(checklist_master_cat['MasterCategory'] + ' ' + checklist_master_cat['Category'])

    ignore_list = ['Other EP_EP', 'BGX/INN_EP', 'BGX/INN_MEDLEY_EP', 'Other Non-RX_EP']

    # df_no_trans = df[((df['MasterCategory'] == 'CHC') | ((df['MasterCategory'] == 'Other DPTs') &
    # (df['Category'] == 'PV')) | ((df['MasterCategory'] == 'EP') & df['Category'].isin(ignore_list)))]
    #
    # df = df[~((df['MasterCategory'] == 'CHC') | ((df['MasterCategory'] == 'Other DPTs') & (df['Category'] == 'PV')) |
    #           ((df['MasterCategory'] == 'EP') & df['Category'].isin(ignore_list)))]

    #####
    df_no_trans = df[((df['MasterCategory'] == 'CHC') |
                      (df['MasterCategory'] == 'Other DPTs') |
                      ((df['MasterCategory'] == 'EP') & df['Category'].isin(ignore_list)))]
    df = df[~((df['MasterCategory'] == 'CHC') |
              (df['MasterCategory'] == 'Other DPTs') |

              ((df['MasterCategory'] == 'EP') & df['Category'].isin(ignore_list)))]

    # df['transcoding'] = True
    df_no_trans['transcoding'] = 'False'

    # integrate exception packs
    df_exception = pd.read_csv(r'data/reference_files/exceptional_packs.csv')
    df_exception = df_exception[['Pack_recode', 'NFC123_Recode', 'Unit', 'Correct SU']].copy()
    df_exception = df_exception.rename(columns={'Correct SU': 'SU_C'})
    df_exception['transcoding'] = 'Exception'
    df = df.merge(df_exception, how='left', on=['Pack_recode', 'NFC123_Recode', 'Unit'])
    df['transcoding'] = df['transcoding'].fillna('True')
    df_exception = df[df['transcoding'] == 'Exception'].copy()
    df = df[df['transcoding'] == 'True'].copy()
    df = df.drop(columns=['SU_C'])
    print()

    # df['Master_cat'] = df['MasterCategory'] + ' ' + df['Category']
    # df['MG_NOT_REQUIRED'] = df['Master_cat'].isin(no_mg_required_list)

    # df['is_multi_molecule'] = df['Molecules_recode'].str.contains('[!,&/]', regex=True)
    # df['is_multi_molecule'][df['Molecules_recode'].str.contains('UNKNOWN', case=False)] = ''

    start_time = time.time()
    df['Pack_recode'] = df['Pack_recode'].replace({
        'COMPRIMIDO': 'TABLET', "`S|'S|UOU": '',
        '([A-z]+)X(\\d+)': '\\1 X \\2', '(\\d+) (0$)': '\\1\\2',
        '  ': ' ', '\\/[ ]*': '/ ', '(\\d+)[ ]*MIE': '\\1000000 IU',
        '(\\d+)[ ]*IE': '\\1 IU',
        'CADA': 'EACH',
        'UNIDADES': 'UNITS',
        'GRATIS': 'FREE',
        'DOSIS|DOSERING|DOSER': 'DOSE',
        'OMEGA 3': 'OMEGA3',
        'OMEGA 3 6 9': 'OMEGA369',
        ' DE ': ' IN ', ',': '.', ' (\\.\\d+)': ' 0\\1',
        'FRASCOS': 'BOTTLES', 'MI[CK]ROG(\\b)': 'MCG\\1',
        '(^|\\W)(CAP\\w+)': '\\1CAPS',
        '(^|\\W)TAB[A-z\\.]* ': '\\1TABLET ', 'REVESTIDO': 'COATED',
        '(\\d+) PLUS': '\\1_PLUS',
        '(\\b)(\\d+ )(\\d+\\s*MG)': '\\1\\2MG \\3',
        'MIKROGRAM|MICROGRAM|MKG|MIKRG': 'MCG',
        '(?<=[\\d ])GM(?=\\W|$)': 'G',
        '(?<=[\\d ])GRAMS?(?=\\W|$)': 'G',
        '(?<=[\\d ])MILLIGRAMS?(?=\\W|$)': 'MG',
        # '([A-z]+)X(\\d+)': '\\1 X \\2',
        '(\\d+)[ ]*X[ ]*((?:\\d[\\.\\d]*|\\d*\\.\\d+)[ ]*[A-z]+)': '\\2 X \\1',
        '(\\W)#([1-9]\\d*)(\\b)': '\\1\\2\\3',
        '\\WN(\\d+)(?=$)': ' \\1', '\\W(\\d+)[ ]*DOZ\\W': ' \\1',
        '\\W(\\d+)[ ]*DOZEN\\W': ' \\1 X 12 ', '(?<=[\\d\\s])?([XN])(?=\\d+)': ' \\1 ', '(\\d+)(?=Vial)': ' \\1 ',
        # '(\\b\\d+) (\\d+[ ]*MC?G)': '\\1\\/\\2',
        '([a-zA-Z])(\\d+\\s*(?:(MC?G|K|G|UI|MU|Y|IU)(?=\\W|$)))': '\\1 \\2',  # FL15MG->FL 15MG
        '([a-zA-Z])(\\d+\\s*(?:(ML|LT?)(?=\\W|$)))': '\\1 \\2',  # FL15MG->FL 15MG
        '(\\d+)H': ' \\1 H',  # Tab 60H -> Tab 60 H
        '(\\d+)UD': ' \\1 UD',  # tab 20 MG 200UD-> tab 20 MG 200 UD
        '(\\d+)ST': ' \\1 ST',  # tab 100MG 100ST-> tab 100MG 100 ST
        '(X\\s*)(\\d+\\-\\d+)': '\\1 '   # VIAL DRY SD 1G 10 V X 10284-26 PRE -> VIAL DRY SD 1G 10 V X PRE
    }, regex=True)

    df['mg'] = df['Pack_recode'].apply(mg_vals)
    df['gram'] = df['Pack_recode'].apply(gram_vals)
    df['ml'] = df['Pack_recode'].apply(ml_vals)
    df['per_ml'] = df['Pack_recode'].apply(per_ml_vals)
    df['units'] = df['Pack_recode'].apply(unit_vals)
    df['dosage'] = df['Pack_recode'].apply(dosage_vals)
    df['multiplier'] = df['Pack_recode'].apply(multiplier_vals)

    # merge transcoding and filtered (not coded)
    df_cols = set(df.columns)
    df_no_trans_cols = set(df_no_trans.columns)
    new_cols = list(df_cols - df_no_trans_cols)
    df_no_trans[new_cols] = ''
    df = pd.concat([df, df_no_trans], ignore_index=True, axis=0)
    # df_no_trans[new_cols] = ''
    df = pd.concat([df, df_exception], ignore_index=True, axis=0)

    df_cat5 = pd.read_excel('data/cat5.xlsx', sheet_name='SU')
    df = df.merge(df_cat5, how='left', on='Category')

    end_time = time.time()
    df.to_csv('data/output/Coded.csv', index=False)
    print(end_time - start_time)


if __name__ == '__main__':
    main()
