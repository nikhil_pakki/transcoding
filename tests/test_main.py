import pytest
from main import ml_vals, mg_vals, per_ml_vals, unit_vals, multiplier_vals


@pytest.mark.parametrize('a, b', [(ml_vals('FERT.SPR.S.C 300MG 2 2ML'), '2.00'),
                                  (ml_vals('CAP 30'), ''),
                                  (ml_vals('DAY AND NIGHT GIFT SET 50ML PLUS 50ML'), '50.00;50.00'),
                                  (ml_vals('S.DRY 100MG 5ML 20ML'), '5.00;20.00'),
                                  (ml_vals('INJ/ INF KONS 75ML 1ML 10 10ML'), '75.00;1.00;10.00'),
                                  (ml_vals('4% DETERGENT 4.5 LT'), '4500.00'),
                                  (ml_vals('LIQUID 1.5 % 5 L X 1 (/15%)'), '5000.00'),
                                  ])
def test_ml_vals(a: str, b: str) -> None:
    """
    Testing the mg_vals function
    :param a: mg_vals(str)
    :param b: str
    :return: None
    """
    assert a == b


@pytest.mark.parametrize('a, b', [(mg_vals('FERT.SPR.S.C 300MG 2 2ML'), '300'),
                                  (mg_vals('CAP 30'), ''),
                                  (mg_vals('DAY AND NIGHT GIFT SET 50MG PLUS 50MG'), '50;50'),
                                  (mg_vals('S.DRY 100MG 5MG 20ML'), '100;5'),
                                  (mg_vals('.VIAL DRY SD.10MCG.1 0818'), '0.0001'),
                                  (mg_vals('VIAFLEX 40G 1000ML X 12'), ''),
                                  (mg_vals('RESPIMAT INH 2.5Y 60'), '0.0025'),
                                  (mg_vals('VIAL INF 100/50 MG 1 (IER)'), '100,50'),
                                  (mg_vals('VIAL INF 100/50/10 1 (IER)'), '100,50,10')
                                  # (mg_vals(''), '0.0001')
                                  ])
def test_mg_vals(a: str, b: str) -> None:
    """
    Testing the mg_vals function
    :param a: mg_vals(str)
    :param b: str
    :return: None
    """
    assert a == b


@pytest.mark.parametrize('a, b', [(per_ml_vals('FERT.SPR.S.C 300MG 2 2ML'), ''),
                                  (per_ml_vals('CAP 30'), ''),
                                  (per_ml_vals('CARTUCCIA 100IU/ 1ML 5 3ML'), '1'),
                                  (per_ml_vals('INJ PEN 6 MG 3 ML X 1 (/ 1ML)'), '1'),
                                  (per_ml_vals('INJ PEN 6 MG 3 ML X 1 (/ML)'), '1'),
                                  (per_ml_vals('INJ PEN 6 MG 3 ML X 1 (/5ML)'), '5'),
                                  (per_ml_vals('VIAL 50 MG / 2ML 2 ML'), '2'),
                                  (per_ml_vals('INJ 25000T(10 000)/ 0.4ML ANTI XA/ 1ML N2 EELT. SÃƒÅ’STEL'), '0.4,1'),
                                  (per_ml_vals('3MG/ 1ML/ ML AURIC 5ML BT 1'), '1,1'),
                                  (per_ml_vals('VIAL INF 100MG / 16.16.7ML 1 (IER)'), '16.16.7')
                                  ])
def test_per_ml_vals(a: str, b: str) -> None:
    """
    Testing the mg_vals function
    :param a: mg_vals(str)
    :param b: str
    :return: None
    """
    assert set(a) == set(b)


@pytest.mark.parametrize('a, b', [(unit_vals('FERT.SPR.S.C 300MG 2 2ML'), 2),
                                  (unit_vals('CAP 30'), 30),
                                  (unit_vals('INJ FL TROCK 100MG'), -1),
                                  (unit_vals('INJ PEN 6 MG 3 ML X 1 (/ 1ML)'), 1),
                                  (unit_vals('F.C. TABLET 200MG 10 X 10'), 100),
                                  (unit_vals('F.C. TABLET 200MG 10 10'), 100),
                                  (unit_vals('FIL.C.TABLET 200 MG X 10 (/ 87'), 10),
                                  # (unit_vals('FIL.C.TABLET 200 MG X 10X10'), 100),
                                  # (unit_vals('FIL.C.TABLET 200 MG X 10 X10'), 100),
                                  # (unit_vals('FIL.C.TABLET 200 MG X10X10'), 100),
                                  # (unit_vals('FIL.C.TABLET 200 MG 10 + 20'), 30),
                                  ])
def test_unit_vals(a: str, b: str) -> None:
    """
    Testing the mg_vals function
    :param a: mg_vals(str)
    :param b: str
    :return: None
    """
    assert str(a) == str(b)


@pytest.mark.parametrize('a, b', [(multiplier_vals('I.SC 25 100U / 1ML 10ML'), ''),
                                  (multiplier_vals('DICODEIN TABLET 30MG 100'), ''),
                                  (multiplier_vals('TABLET 81MG 10 X 10'), '10,10'),
                                  (multiplier_vals('CAP 100MG 28X5'), '28,5'),
                                  (multiplier_vals('KPIN D500 10 X 28'), '10,28'),
                                  (multiplier_vals('TABLET 2 MG X 10 X3'), '10,3'),
                                  (multiplier_vals('IJVS.FF IJP 40 MG FYLPN 1 ST X 2'), '1,2'),
                                  ])
def test_multiplier_vals(a: str, b: str) -> None:
    """
    Testing the mg_vals function
    :param a: mg_vals(str)
    :param b: str
    :return: None
    """
    assert set(a) == set(b)

#
# if __name__ == '__main__':
#     pytest.main()
