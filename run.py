import main
import process_su


# main.main(raw_file_path=r'D:\office_data\office_data\su_project\su_regex\data\cleaned_dmt_ref_transcode.csv')
# 'data/output/Coded.csv'

# raw_file_path = r'data/output/su_mismatch_dmt_ref_transcode_16_aug.csv'
# raw_file_path = r'data/test_check_3_oct.csv'
raw_file_path = r'D:\office_data\office_data\su_project\su_regex\data\cleaned_dmt_ref_transcode.csv'

main.main(raw_file_path=raw_file_path)

process_su.main(raw_file_path=r'data/output/Coded.csv')  # 'data/output/process_su_output.csv'
